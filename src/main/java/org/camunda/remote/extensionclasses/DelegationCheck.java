package org.camunda.remote.extensionclasses;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.lang.management.ManagementFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.management.OperatingSystemMXBean;

/**
 * Created by andres on 05/03/18.
 */
public class DelegationCheck implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {

        OperatingSystemMXBean bean = (com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();

        double load = bean.getProcessCpuLoad();

        execution.setVariable("delegate", load > 0.5);

    }
}
