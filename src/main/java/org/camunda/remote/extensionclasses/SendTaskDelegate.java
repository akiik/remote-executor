package org.camunda.remote.extensionclasses;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.impl.util.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

public class SendTaskDelegate implements JavaDelegate {
    private final static Logger LOGGER = Logger.getLogger("SendTaskDelegate");
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        execution.getProcessDefinitionId();

        String deploymentId = execution.getProcessEngineServices().getRepositoryService().createDeploymentQuery().singleResult().getId();

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost createDeployment = new HttpPost(execution.getVariable("workerUrl") + "/deployment/create");
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addTextBody("deployment-name", "workerDef2", ContentType.TEXT_PLAIN);
        builder.addTextBody("deployment-source", "process application", ContentType.TEXT_PLAIN);

        String resourceName = execution.getCurrentActivityId().replace("send_task", "external.bpmn");
        File initialFile = new File(System.getProperty("user.home") + System.getProperty("file.separator") + resourceName);
        InputStream targetStream = new FileInputStream(initialFile);

        builder.addBinaryBody(
                "data",
                targetStream,
                ContentType.APPLICATION_OCTET_STREAM,
                resourceName
        );

        HttpEntity multipart = builder.build();
        createDeployment.setEntity(multipart);
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(createDeployment);
        } catch (IOException e) {
            e.printStackTrace();
        }


        HttpEntity responseEntity = response.getEntity();
        String retSrc = EntityUtils.toString(responseEntity);

        JSONObject json = new JSONObject(retSrc);
        LOGGER.info(json.toString());
        JSONObject definitions = json.getJSONObject("deployedProcessDefinitions");
        String definitionId = definitions.keys().next().toString();

        //start the process
        HttpPost startProcess = new HttpPost(execution.getVariable("workerUrl") + "/process-definition/" + definitionId + "/start");

        //assign the variables
        String masterProcessIdVariable = "{\"value\":\"" + execution.getProcessInstanceId() + "\", \"type\": \"String\"}";
        String masterUrlVariable = "{\"value\":\"" + execution.getVariable("masterUrl") + "\", \"type\": \"String\"}";

        String jsonContent =
            "{\"variables\":{"
                + "\"masterProcessId\":" + masterProcessIdVariable
                + ",\"masterUrl\":" + masterUrlVariable
            + "},\"businessKey\":\"\"}";

        StringEntity entity = new StringEntity(jsonContent);

        startProcess.setEntity(entity);
        startProcess.setHeader("Content-type", "application/json");

        CloseableHttpResponse startProcessResponse = httpClient.execute(startProcess);

        LOGGER.info("Status Code ::" + startProcessResponse.getStatusLine().getStatusCode());

        httpClient.close();

    }
}
