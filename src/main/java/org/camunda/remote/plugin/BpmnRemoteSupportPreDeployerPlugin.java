package org.camunda.remote.plugin;

import org.camunda.bpm.engine.impl.cfg.AbstractProcessEnginePlugin;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.persistence.deploy.Deployer;

import java.util.ArrayList;
import java.util.List;

public class BpmnRemoteSupportPreDeployerPlugin extends AbstractProcessEnginePlugin {

    @Override
    public void preInit(ProcessEngineConfigurationImpl processEngineConfiguration) {

        // get all existing deployers
        List<Deployer> deployers = processEngineConfiguration.getCustomPreDeployers();

        if(deployers == null) {
            // if no deployer exists, create new list
            deployers = new ArrayList<Deployer>();
            processEngineConfiguration.setCustomPreDeployers(deployers);
        }

        deployers.add(new BpmnRemoteSupportPreDeployer());

    }
}