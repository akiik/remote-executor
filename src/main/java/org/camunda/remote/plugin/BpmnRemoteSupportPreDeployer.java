package org.camunda.remote.plugin;

import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.persistence.entity.DeploymentEntity;
import org.camunda.bpm.engine.impl.persistence.entity.ResourceEntity;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.instance.Process;

import java.util.logging.Logger;

public class BpmnRemoteSupportPreDeployer implements org.camunda.bpm.engine.impl.persistence.deploy.Deployer {

    private final static Logger LOGGER = Logger.getLogger("BpmnRemoteSupportPreDeployer");

    @Override
    public void deploy(DeploymentEntity deployment) {

        //decompose the original model
        BpmnRemoteSupportDecomposer decomposer = new BpmnRemoteSupportDecomposer(deployment);

        decomposer.decompose();
        decomposer.writeToFile();

        LOGGER.info(Bpmn.convertToString(decomposer.getExtendedModel()));

        //replace the original model with the extended one
        ResourceEntity resource = deployment.getResources().values().iterator().next();
        resource.setBytes(Bpmn.convertToString(decomposer.getExtendedModel()).getBytes());

    }
}
