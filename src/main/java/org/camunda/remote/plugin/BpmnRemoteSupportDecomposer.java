package org.camunda.remote.plugin;

import org.camunda.bpm.engine.impl.persistence.entity.DeploymentEntity;
import org.camunda.bpm.engine.impl.persistence.entity.ResourceEntity;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.*;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.camunda.bpm.model.bpmn.instance.camunda.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;



public class BpmnRemoteSupportDecomposer {

    private DeploymentEntity deployment;
    private BpmnModelInstance originalModel;
    private BpmnModelInstance extendedModel;
    private List<BpmnModelInstance> externalModels = new ArrayList<>();

    private final static Logger LOGGER = Logger.getLogger("BpmnRemoteSupportDecomposer");

    public BpmnRemoteSupportDecomposer(DeploymentEntity deployemnt) {
        this.deployment = deployemnt;
    }

    public DeploymentEntity getDeployment() {
        return deployment;
    }

    public BpmnModelInstance getOriginalModel() {
        return originalModel;
    }

    public  BpmnModelInstance getExtendedModel() {
        return extendedModel;
    }

    public List<BpmnModelInstance> getExternalModels() {
        return externalModels;
    }

    public void decompose() {
        Map<String, ResourceEntity> resources = deployment.getResources();

        String resourceName = resources.values().iterator().next().getName();

        originalModel =
                Bpmn.readModelFromStream(
                        BpmnRemoteSupportDecomposer
                                .class
                                .getClassLoader()
                                .getResourceAsStream(resourceName)
                );

        extendedModel = originalModel.clone();
        Collection<FlowNode> nodes = extendedModel.getModelElementsByType(FlowNode.class);

        nodes.stream().forEach((node) -> {
            if (isExternalNode(node)) {
                handleExternalNode(node);
                externalModels.add(createExternalModelInstance(node));
            }
        });

    }

    static boolean isExternalNode(FlowNode node) {
        boolean result = false;

        ExtensionElements extensionElements = node.getExtensionElements();

        if (extensionElements != null && extensionElements.getChildElementsByType(CamundaProperties.class).size() > 0) {
            CamundaProperties camundaProperties = extensionElements
                    .getElementsQuery()
                    .filterByType(CamundaProperties.class)
                    .singleResult();

            result = camundaProperties
                    .getCamundaProperties()
                    .stream()
                    .anyMatch((property) -> (property.getCamundaName().equals("type") &&
                            property.getCamundaValue().equals("external")));
        }

        return result;
    }

    protected <T extends BpmnModelElementInstance> T createElement(
            BpmnModelElementInstance parentElement,
            String id,
            Class<T> elementClass
    ) {
        T element = extendedModel.newInstance(elementClass);
        element.setAttributeValue("id", id, true);
        parentElement.addChildElement(element);
        return element;
    }


    public SequenceFlow createSequenceFlow(
            Process process,
            FlowNode from,
            FlowNode to
    ) {
        String identifier = from.getId() + "-" + to.getId();
        SequenceFlow sequenceFlow = createElement(process, identifier, SequenceFlow.class);
        process.addChildElement(sequenceFlow);
        sequenceFlow.setSource(from);
        from.getOutgoing().add(sequenceFlow);
        sequenceFlow.setTarget(to);
        to.getIncoming().add(sequenceFlow);
        return sequenceFlow;
    }

    protected void handleExternalNode(
            FlowNode externalNode
    ) {
        Process process = extendedModel.getModelElementsByType(Process.class).iterator().next();
        SequenceFlow incomingSequenceFlow = externalNode.getIncoming().iterator().next();
        SequenceFlow outgoingSequenceFlow = externalNode.getOutgoing().iterator().next();

        //create service task for getting the external variable value
        ServiceTask checkIfDelegate = createElement(
                process,
                externalNode.getId() + "_checkIfDelegate",
                ServiceTask.class
        );

        //create XOR split
        ExclusiveGateway XORsplit = createElement(
                process,
                externalNode.getId() + "_XOR_split",
                ExclusiveGateway.class
        );

        //create send task
        SendTask sendTask = createElement(
                process,
                externalNode.getId() + "_send_task",
                SendTask.class
        );

        //create receive task
        ReceiveTask receiveTask = createElement(
                process,
                externalNode.getId() + "_receive_task",
                ReceiveTask.class
        );

        //create XOR join
        ExclusiveGateway XORjoin = createElement(
                process,
                externalNode.getId() + "_XOR_join",
                ExclusiveGateway.class
        );

        ConditionExpression as = extendedModel.newInstance(ConditionExpression.class);
        as.setType("bpmn:tFormalExpression");
        as.setTextContent("${delegate}");
        as.setId(externalNode.getId() + "_condition");

        //message creation
        Message workerDoneMessage = extendedModel.newInstance(Message.class);
        workerDoneMessage.setId("workerDone");
        workerDoneMessage.setName("workerDone");

        extendedModel.getDefinitions().insertElementAfter(workerDoneMessage, null);
        receiveTask.setMessage(workerDoneMessage);

        //create sequence flow from original incoming to checkIfDelegate
        SequenceFlow incomingToDelegate = createSequenceFlow(process, incomingSequenceFlow.getSource(), checkIfDelegate);
        //create sequence flow from checkIfDelegate to XOR split
        SequenceFlow delegateToXORsplit = createSequenceFlow(process, checkIfDelegate, XORsplit);
        //create sequence flow from XOR split to send task
        SequenceFlow XORsplitToSendTask = createSequenceFlow(process, XORsplit, sendTask);
        //create sequence flow from XOR split to original node
        SequenceFlow XORsplitToOriginalNode = createSequenceFlow(process, XORsplit, externalNode);
        //create sequence flow from send task to receive task
        SequenceFlow sendTaskToReceiveTask = createSequenceFlow(process, sendTask, receiveTask);
        //create sequence flow from receive task to XOR join
        SequenceFlow receiveTaskToXORjoin = createSequenceFlow(process, receiveTask, XORjoin);
        //create sequence flow from original node to XOR join
        SequenceFlow originalToXORjoin = createSequenceFlow(process, externalNode, XORjoin);
        //create sequence flow from XOR join to next node
        SequenceFlow XORjoinToNext = createSequenceFlow(process, XORjoin, outgoingSequenceFlow.getTarget());

        //add nodes to process
        process.addChildElement(XORsplit);
        process.addChildElement(sendTask);
        process.addChildElement(receiveTask);
        process.addChildElement(XORjoin);


        process.addChildElement(incomingToDelegate);
        process.addChildElement(delegateToXORsplit);
        process.addChildElement(XORsplitToSendTask);
        process.addChildElement(XORsplitToOriginalNode);
        process.addChildElement(sendTaskToReceiveTask);
        process.addChildElement(receiveTaskToXORjoin);
        process.addChildElement(originalToXORjoin);
        process.addChildElement(XORjoinToNext);

        //add attributes
        sendTask.setCamundaClass("org.camunda.remote.extensionclasses.SendTaskDelegate");
        sendTask.setCamundaAsyncBefore(true);
        checkIfDelegate.setCamundaClass("org.camunda.remote.extensionclasses.DelegationCheck");
        XORsplit.setDefault(XORsplitToOriginalNode);
        XORsplitToSendTask.addChildElement(as);

        //remove unnecessary flows
        process.removeChildElement(outgoingSequenceFlow);
        process.removeChildElement(incomingSequenceFlow);

    }

    public BpmnModelInstance createExternalModelInstance(FlowNode externalNode) {

        BpmnModelInstance externalModel = originalModel.clone();
        FlowNode clonedExternalNode = externalModel.getModelElementById(externalNode.getId());
        Process process = externalModel.getModelElementsByType(Process.class).iterator().next();
        Collection<FlowNode> flowNodes = externalModel.getModelElementsByType(FlowNode.class);

        flowNodes.forEach((node) -> {
            if (!(node.getId().equals(clonedExternalNode.getId()) ||
                    node.getElementType().getTypeName().equals("startEvent") ||
                    node.getElementType().getTypeName().equals("endEvent"))) {
                removeChild(process, node);
            }
        });

        if (!clonedExternalNode.getIncoming().iterator().hasNext()) {
            SequenceFlow sequenceFlow = externalModel.newInstance(SequenceFlow.class);
            sequenceFlow.setAttributeValue("id", "start-to-external", true);
            process.addChildElement(sequenceFlow);

            sequenceFlow.setSource(externalModel.getModelElementsByType(StartEvent.class).iterator().next());
            sequenceFlow.setTarget(clonedExternalNode);
            clonedExternalNode.getIncoming().add(sequenceFlow);

        }
        // add a sequence flow from startEvent to node if not exist
        if (!clonedExternalNode.getOutgoing().iterator().hasNext()) {
            SequenceFlow sequenceFlow = externalModel.newInstance(SequenceFlow.class);
            sequenceFlow.setAttributeValue("id", "external-to-end", true);
            process.addChildElement(sequenceFlow);

            sequenceFlow.setSource(clonedExternalNode);
            sequenceFlow.setTarget(externalModel.getModelElementsByType(EndEvent.class).iterator().next());
            clonedExternalNode.getOutgoing().add(sequenceFlow);
        }

        process.setId(externalNode.getId() + "_external");

        //connector creation
        CamundaConnector connector = externalModel.newInstance(CamundaConnector.class);

        CamundaConnectorId connectorId = externalModel.newInstance(CamundaConnectorId.class);
        connectorId.setTextContent("http-connector");
        connector.setCamundaConnectorId(connectorId);
        CamundaInputOutput inputOutput = externalModel.newInstance(CamundaInputOutput.class);

        CamundaInputParameter url = externalModel.newInstance(CamundaInputParameter.class);
        CamundaInputParameter method = externalModel.newInstance(CamundaInputParameter.class);
        CamundaInputParameter headers = externalModel.newInstance(CamundaInputParameter.class);
        CamundaInputParameter payload = externalModel.newInstance(CamundaInputParameter.class);
        url.setCamundaName("url");
        method.setCamundaName("method");
        headers.setCamundaName("headers");
        payload.setCamundaName("payload");
        url.setTextContent("${masterUrl}/message");
        method.setTextContent("POST");

        String resultVariable = "{\"value\":\"${result}\", \"type\": \"String\"}";

        String jsonContent =
                "{\"messageName\": \"workerDone\"," +
                "\"processInstanceId\":\"${masterProcessId}\"," +
                "\"processVariables\":{"
                        + "\"result\":" + resultVariable
                        + "}" +
                "}";

        payload.setTextContent(jsonContent);

        CamundaMap map = externalModel.newInstance(CamundaMap.class);
        CamundaEntry entry = externalModel.newInstance(CamundaEntry.class);
        entry.setCamundaKey("Content-Type");
        entry.setTextContent("application/json");
        map.addChildElement(entry);
        headers.getDomElement().appendChild(map.getDomElement());

        inputOutput.addChildElement(url);
        inputOutput.addChildElement(method);
        inputOutput.addChildElement(headers);
        inputOutput.addChildElement(payload);

        connector.setCamundaInputOutput(inputOutput);
        EndEvent end = externalModel.getModelElementsByType(EndEvent.class).iterator().next();
        ExtensionElements extensions = externalModel.newInstance(ExtensionElements.class);
        extensions.addChildElement(connector);
        Message message = externalModel.newInstance(Message.class);
        message.setId("workerDone");
        message.setName("workerDone");
        MessageEventDefinition eventDefinition = externalModel.newInstance(MessageEventDefinition.class);
        process.getParentElement().addChildElement(message);
        eventDefinition.setMessage(message);
        eventDefinition.addChildElement(extensions);

        end.addChildElement(eventDefinition);

        ExtensionElements extensionsForEnd = externalModel.newInstance(ExtensionElements.class);
        CamundaExecutionListener listen = externalModel.newInstance(CamundaExecutionListener.class);
        CamundaScript script = externalModel.newInstance(CamundaScript.class);
        script.setCamundaScriptFormat("groovy");
        script.setTextContent("println 'End Event Start! Result: ' + result");
        listen.setCamundaEvent("start");
        listen.addChildElement(script);
        extensionsForEnd.addChildElement(listen);

        return externalModel;
    }

    public Process removeChild(Process process, FlowNode node){

        Collection<SequenceFlow> incoming = node.getIncoming();
        Collection<SequenceFlow> outgoing = node.getOutgoing();

        incoming.forEach((flow) -> process.removeChildElement(flow));
        outgoing.forEach((flow) -> process.removeChildElement(flow));

        process.removeChildElement(node);

        return process;
    }

    public void writeToFile() {
        try {
            // Write extended model
            LOGGER.info("Writing extended model to file");
            String resourceName = deployment.getResources().values().iterator().next().getName();
            LOGGER.info("Resource name :: " + resourceName);
            File file = new File(System.getProperty("user.home") + System.getProperty("file.separator") + "extended_" + resourceName);
            LOGGER.info("Writing extended model to file:: " + file.getName());
            file.createNewFile();
            extendedModel.getDefinitions().getBpmDiagrams().clear();
            Bpmn.writeModelToFile(file, extendedModel);

            // Write external models
            LOGGER.info("Writing external model to file");
            externalModels.stream().forEach((model) -> {
                String fn = model.getModelElementsByType(Process.class).iterator().next().getId();
                File f = new File(System.getProperty("user.home") + System.getProperty("file.separator") + fn + ".bpmn");
                model.getDefinitions().getBpmDiagrams().clear();
                LOGGER.info("Trying to write file::" + fn);
                LOGGER.info(Bpmn.convertToString(model));
                try {
                    f.createNewFile();
                    Bpmn.writeModelToFile(f, model);
                } catch (IOException e) {
                    e.printStackTrace();
                    LOGGER.warning("Error while writing to file");
                }
            });
        } catch (IOException e) {
            LOGGER.warning("Error while writing to file");
        }



    }
}
