package org.camunda.remote.app;

import org.camunda.bpm.engine.impl.cfg.ProcessEnginePlugin;
import org.camunda.connect.plugin.impl.ConnectProcessEnginePlugin;
import org.camunda.remote.plugin.BpmnRemoteSupportPreDeployerPlugin;
import org.camunda.spin.plugin.impl.SpinProcessEnginePlugin;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyConf {

    @ConditionalOnClass(ConnectProcessEnginePlugin.class)
    @Configuration
    static class ConnectConfiguration {

        @Bean
        @ConditionalOnMissingBean(name = "connectProcessEnginePlugin")
        public static ProcessEnginePlugin connectProcessEnginePlugin() {
            return new ConnectProcessEnginePlugin();
        }
    }

    @ConditionalOnClass(BpmnRemoteSupportPreDeployerPlugin.class)
    @Configuration
    static class RemoteConfiguration {

        @Bean
        @ConditionalOnMissingBean(name = "remoteProcessEnginePlugin")
        public static ProcessEnginePlugin remoteProcessEnginePlugin() {
            return new BpmnRemoteSupportPreDeployerPlugin();
        }
    }

    @ConditionalOnClass(SpinProcessEnginePlugin.class)
    @Configuration
    static class SpinConfiguration {

        @Bean
        @ConditionalOnMissingBean(name = "spinProcessEnginePlugin")
        public static ProcessEnginePlugin spinProcessEnginePlugin() {
            return new SpinProcessEnginePlugin();
        }
    }

}
