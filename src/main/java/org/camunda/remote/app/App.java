package org.camunda.remote.app;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

@SpringBootApplication
@EnableProcessApplication
public class App {

    private final static Logger LOGGER = Logger.getLogger("App");

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}